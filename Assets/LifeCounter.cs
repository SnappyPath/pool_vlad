﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LifeCounter : MonoBehaviour
{
    public TextMeshProUGUI lifeText;
    public int lifeCount = 3;
    void Start()
    {
        lifeText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        lifeText.text = "Health: " + lifeCount;

        if (lifeCount <= 0)
        {
            FindObjectOfType<SceneManager>().Death();
        }
    }

    public void Damage()
    {
        lifeCount -= 1;
    }
}

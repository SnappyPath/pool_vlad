﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementGun : MonoBehaviour
{

    //[SerializeField] private GameObject projectile;
    [SerializeField] private GameObject aim;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Mouse();


        if (Input.GetMouseButtonDown(0))
        {
            Fire();
        }

    }

    void Fire()
    {
        //FindObjectOfType<AudioManager>().PlaySound("Shooting");
        //Instantiate(projectile, aim.transform.position, aim.transform.rotation);
        FindObjectOfType<Pool>().Get("Bullet").transform.position = aim.transform.position;
    }

    private void Mouse()
    {
        /*var mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        var angle = Vector2.Angle(Vector2.right, mousePosition - transform.position);
        transform.eulerAngles = new Vector3(0f, 0f, transform.position.y < mousePosition.y ? angle : -angle);*/

        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        transform.up = direction;
    }

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    // Start is called before the first frame update

    public float minSecondsBetweenSpawning = 3.0f;
    public float maxSecondsBetweenSpawning = 6.0f;

    private float savedTime;
    private float secondsBetweenSpawning;
    void Start()
    {
        secondsBetweenSpawning = Random.Range(minSecondsBetweenSpawning, maxSecondsBetweenSpawning);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - savedTime >= secondsBetweenSpawning)
        {
            MakeThingToSpawn();
            savedTime = Time.time;
            secondsBetweenSpawning = Random.Range(minSecondsBetweenSpawning, maxSecondsBetweenSpawning);
        }
    }

    void MakeThingToSpawn()
    {
        FindObjectOfType<Pool>().Get("Enemy").transform.position = transform.position;
    }
}

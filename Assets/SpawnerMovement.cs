﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerMovement : MonoBehaviour
{
    [SerializeField] private bool goBack;
    [SerializeField] private float speed;
    [SerializeField] private Vector3 targetPosition;
    [SerializeField] private Vector3 targetRotation;


    private Vector3 startPosition;
    private Vector3 keeper;
    private float pathLength;

    private void Awake()
    {
        startPosition = transform.position;
        targetPosition += startPosition;
    }

    private void Update()
    {

        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        transform.Rotate(targetRotation * speed * Time.deltaTime);
        pathLength = Vector3.Distance(transform.position, targetPosition);

        if (goBack && pathLength == 0)
        {
            keeper = targetPosition;
            targetPosition = startPosition;
            startPosition = keeper;
            targetRotation *= -1;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
   public GameObject StartState, GameplayState, DeathState;

    /*public enum myEnum
    {
        StartState,
        GameplayState,
        DeathState
    };*/

    //GameObject a = myEnum.StartState;

    private string currentState;

    void Start()
    {
        ChangeState("StartState");
        StartSettings();
    }

    public void ChangeState(string canvasName)
    {
        switch (canvasName)
        {
            case "StartState":
                StartState.SetActive(true);
                GameplayState.SetActive(false);
                DeathState.SetActive(false);
                break;
            case "GameplayState":
                StartState.SetActive(false);
                GameplayState.SetActive(true);
                DeathState.SetActive(false);
                break;
            case "DeathState":
                StartState.SetActive(false);
                GameplayState.SetActive(false);
                DeathState.SetActive(true);
                break;
        }
        currentState = canvasName;
    }


    public void StartSettings()
    {
        Time.timeScale = 0;
    }
    public void Death()
    {
        ChangeState("DeathState");
        Time.timeScale = 0;
    }

    public void NewGame()
    {
        FindObjectOfType<Pool>().Return("Enemy");
        FindObjectOfType<LifeCounter>().lifeCount = 3;
        Time.timeScale = 1;
    }
}

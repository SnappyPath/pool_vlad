﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public Renderer m_Renderer;

    public float skorostPadenia = 10f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.Translate(-Vector2.up * Time.deltaTime * skorostPadenia, Space.World);

        {
            if (m_Renderer.isVisible)
            {
                //Debug.Log("Object is visible");
            }
            else
            {
                FindObjectOfType<Pool>().Return("Enemy");
                FindObjectOfType<LifeCounter>().Damage();
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [System.Serializable]
    public class PoolObject
    {
        public GameObject prefab;
        public int amount;
    }

    [SerializeField] [Header("Координаты")] Transform spawner = null;
    public List<PoolObject> poolObjects;
    public List<GameObject> objectsOnScene;
    public static Pool instance;

   


    void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);

        foreach (PoolObject item in poolObjects)
        {
            for (int i = 0; i < item.amount; i++)
            {
                GameObject obj = Instantiate(item.prefab, spawner);
                objectsOnScene.Add(obj);
                obj.SetActive(false);

            }

        }


    }
    public GameObject Get(string tag)
    {
        foreach(GameObject item in objectsOnScene)
        {
            if (item.CompareTag(tag) && !item.activeInHierarchy)
            {
                item.SetActive(true);
                return item;
            }           
        }
        return null;
    }

    public GameObject Return(string tag)
    {
        foreach (GameObject item in objectsOnScene)
        {
            if (item.CompareTag(tag) && item.activeInHierarchy)
            {
                item.SetActive(false);
                return item;
            }
        }
        return null;
    }
}

    


